import os
import requests

print('Введите доменное имя')
domain_name = input()
# ANY +noall +answer

print(f'Представляем отчет об {domain_name}\n') 
print("Выводим информацию об ip адресах доменного имени и всех типах записей DNS")

dig_command = f'dig {domain_name}'
os.system(dig_command)

print("Подробная информация о домене")

whois = f'whois {domain_name}'

os.system(whois)
print("+++++++++++++++++++++++++++++++")
print("поддомены")

# читать все поддомены
file = open("subdomains.txt")
# прочитать весь контент
content = file.read()
# разделить на новые строки
subdomains = content.splitlines()
# список обнаруженных поддоменов
discovered_subdomains = []
for subdomain in subdomains:
    # создать URL
    url = f"http://{subdomain}.{domain_name}"
    try:
        # если возникает ОШИБКА, значит, субдомен не существует
        requests.get(url)
    except requests.ConnectionError:
        # если поддомена не существует, просто передать, ничего не выводить
        pass
    else:
        print("[+] Обнаружен поддомен:", url)
        # добавляем обнаруженный поддомен в наш список
        discovered_subdomains.append(url)

# сохраняем обнаруженные поддомены в файл
with open("discovered_subdomains.txt", "w") as f:
    for subdomain in discovered_subdomains:
        print(subdomain, file=f)
